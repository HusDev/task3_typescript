import React, { Component } from "react";
import Terms from "./components/Terms/Terms";

class App extends Component<{}, { value: string }> {
  render() {
    return (
      <div className="App container">
        <Terms />
      </div>
    );
  }
}

export default App;
