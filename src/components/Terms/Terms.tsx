import React, { Component } from "react";
import "./style.css";
import Content from "../contents/Content";
class Terms extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    // initial state
    this.state = {
      isRead: false,
      isChecked: false
    };
  }

  public handleConfirm(event: any): void {
    this.setState({ isChecked: !this.state.isChecked });
  }

  public handleScroll(event: any): void {
    console.log("offsetHeight: " + event.target.offsetHeight);
    console.log("scrollTop: " + event.target.scrollTop);
    console.log("scrollHeight: " + event.target.scrollHeight);
    console.log("------------------");

    if (
      event.target.offsetHeight + event.target.scrollTop >=
      event.target.scrollHeight
    ) {
      this.setState({ isRead: true });
      console.log(this.state.isRead);
    }
  }

  render() {
    return (
      <div className="card mt-5">
        <div className="mt-3 mb-4">
          <div style={{ marginLeft: "80px", color: "gray" }} className="mb-5">
            <h2>Please read terms carfully</h2>
          </div>
          <div className="terms" onScroll={e => this.handleScroll(e)}>
            <Content />
          </div>
          {/* form */}
          <form>
            <div className="row mt-4" style={{ width: "84%", margin: "auto" }}>
              <div className="col-md-8 form-group">
                <input
                  onClick={e => this.handleConfirm(e)}
                  className="form-check-input"
                  type="checkbox"
                  name="confirm"
                  id=""
                />
                <label htmlFor="confirm">
                  I've read and accept <a href=""> terms and conditions </a>
                </label>
              </div>
              <div className="col-md-4" style={{ textAlign: "center" }}>
                <button
                  className="btn btn-style"
                  disabled={!(this.state.isRead && this.state.isChecked)}
                >
                  Accept
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default Terms;
