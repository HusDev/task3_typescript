import React, { Component } from "react";
import image from "./t.jpg";
class Content extends Component {
  render() {
    return (
      <div>
        <h1>These are terms and conditions</h1>

        <h2>First Header</h2>
        <p>
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ab maiores
          quia sed eos optio. Ratione ab similique fuga quod blanditiis eum
          nesciunt dolores sequi fugiat adipisci velit corporis, excepturi
          fugit!
        </p>
        <img src={image} alt="okoko" />
        <h2>Second Header</h2>

        <p>
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ab maiores
          quia sed eos optio. Ratione ab similique fuga quod blanditiis eum
          nesciunt dolores sequi fugiat adipisci velit corporis, excepturi
          fugit!
        </p>
        <h2>Third Header</h2>

        <p>
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ab maiores
          quia sed eos optio. Ratione ab similique fuga quod blanditiis eum
          nesciunt dolores sequi fugiat adipisci velit corporis, excepturi
          fugit!
        </p>
        <h2>Fourth Header</h2>

        <p>
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ab maiores
          quia sed eos optio. Ratione ab similique fuga quod blanditiis eum
          nesciunt dolores sequi fugiat adipisci velit corporis, excepturi
          fugit!
        </p>
      </div>
    );
  }
}

export default Content;
